CREATE EXTENSION postgis;

ALTER TABLE city
ADD location GEOMETRY NOT NULL;
