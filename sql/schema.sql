-- user is reserved keyword
CREATE TABLE users (
    username VARCHAR(64) PRIMARY KEY,
    password VARCHAR(255) NOT NULL

);

CREATE TABLE city (
    city_id INTEGER PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE metric (
    metric_id INTEGER PRIMARY KEY,
    city_id INTEGER REFERENCES city(city_id) NOT NULL,
    date_created TIMESTAMP NOT NULL,
    type TEXT NOT NULL,
    value DOUBLE PRECISION NOT NULL
);
