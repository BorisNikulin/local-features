```plantuml
@startuml
skinparam titleFontSize 30

title __Project Change Lifecycle__\n

partition Requirements/Design {
    start

    :Submit Issue;

    repeat
        :Pinpoint issue;
        :Apply appropriate lables;
    repeat while (more questions?) is (yes)

    if (Is a valid issue?) then (no)
        stop
    endif

    repeat
        :Plan issue;
        :Apply appropriate lables;
    repeat while (more questions?) is (yes)
}

partition Managment {
    :Assign issue to milestone (sprint)
    to set deadlines;
    :Assign issue to developer;
    note right
        One can assign themselves
        or the current manager can.
    end note
}

partition Coding {
    :Checkout master;
    :Pull;
    :Create a new branch;

    if (Change affects frontend) then (yes)
        partition Frontend {
            :Create designed frontend view;
            :Test static content by directly
            opening file in browser;
            :Create stub endpoint
            serving the view;
            :Inject placeholder model variables;
            note
                When you need data from the server
                create dummy model varaibles.

                ""@GetMapping("<endpoint>")""
                ""public String""
                ""foo(Model model) {""
                    ""model.addAttribute("var1", "dummyData")""
                    ""model.addAttribute("var2", 0)""
                    ...
                    ""return "<view>";""
                ""}""
            end note

            if (Is form?) then (yes)
                :Create another endpoint
                recieving the from object;
                :Return form object in endpoint;
                note
                    Returing the from object
                    from the controller will
                    return the form object as json.

                    ""@PostMapping("<endpoint>")""
                    ""public @ResponseBody FormObject""
                    ""foo(@ModelVariable FormObject form)""
                    ""{ return form; }""
                end note
            endif

            :Test the dynamic content
            served by the sever;
        }
    else if (Change affects backend) then (yes)
        partition Backend {
            :Create designed java code;
            :Create tests;
            :Test functionality;

            if (Change requires a view?) then (yes)
                partition View {
                    if (Already have view?) then (no)
                        while (Needed view is in master?) is (no)
                            :Wait...
                            Implement another issue;
                        endwhile
                        :Merge in newest master;
                    endif
                    :Change dummy endpoint created
                    by author to a real one
                    in the proper class;
                    :Use new backend functionality
                    to compute required model variables;
                    :Add computed variables to the model;
                    note
                        This should replace one or more
                        placeholder variables created
                        by the author of the view
                    end note
                }
            endif
        }
    endif

    :Run the gradle ""check"" task;
    while (Errors?) is (yes)
        :Fix errors;
        :Run the gradle ""check"" task;
    endwhile
}

partition Feedback {
    :Push branch;
    note
        To make your branch a tracking branch
        use ""git push -u origin <branch>"".
        That will allow a subsequent ""git push""
        to know where to push the branch
        (as well as pull).
        You can use ""HEAD"" to refer to
        the current branch.
    end note


    if (Need implementation help?) then (yes)
        partition Help {
            :Open a WIP MR;
            repeat
                :Use help to finish an
                aspect of implementation;
            repeatwhile (Need more help?) is (yes)
            :Remove WIP status on MR;
        }
    endif

    partition Review {
        repeat
            fork
                :Others review MR;
                while (Unresolved threads?) is (yes)
                    :Resolve converns by either
                    persuading their concern is not a concern
                    or change your implementation and push the change;
                endwhile
                :Gain approval;

            fork again
                if (CI?) then (fails)
                    :Fix implementation
                    such that CI passes;
                endif
            end fork
        repeatwhile (No unresolved threads
            and CI passes
            and enough approved) is (no)
    }

    :Merge MR;
    note
        When merging an MR
        make sure you check the option
        for deleting source branch.
        Do not check squash merge if
        using a merge train.
    end note

    stop
}
@enduml
```
