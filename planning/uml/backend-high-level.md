```plantuml
@startuml
abstract class Query

class CityFactory
{
    +byName(cityName : String) : City
}

class MetricManager
{
    +getMetrics(query : Query) : CityMetrics
}

class City
abstract class Metric
class CityMetrics
class Statistics

class Formatter
class UnitConverter
class UserMetricPreferences
class QueryResult

Query ..> CityFactory : queries
Query ..> MetricManager : queries

CityFactory ..> City : creates
MetricManager ..> CityMetrics : fetches
'MetricManager *-- Statistics
Statistics --* MetricManager

CityMetrics "1" *-- "*" Metric
CityMetrics  "1" o-- "1" City

QueryResult *-- CityMetrics
QueryResult *-- Formatter

Formatter "1" o-- "0..1" UserMetricPreferences
Formatter *- UnitConverter
UnitConverter "1" o-- "0..1" UserMetricPreferences
@enduml
```
