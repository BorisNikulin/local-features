```plantuml
@startuml
title Successful User wants to filter metrics

participant User
participant App
database UserStore as db


User -> App ++ : Searches a keyword for metrics and filters are set
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: Searching Metrics

User -> App: Submits Values
App -> App: Verifies data
App -> db: Queries DB
db ->App:Returns Queried data using logic inserted in filter
App -> User: User gets the value


@enduml
```
