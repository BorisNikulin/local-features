```plantuml
@startuml
title Unsuccessful User wants to filter metrics

participant User
participant App
database UserStore as db


User -> App ++ : Searches a keyword for metrics with filter
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: No Metrics searched found

User -> App: Submits Values

App -> User: User gets error. No data found


@enduml
```
