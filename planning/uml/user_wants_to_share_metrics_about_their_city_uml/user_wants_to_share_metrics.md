```plantuml
@startuml
title Successful To share metrics

participant User
participant App
database UserStore as db

User -> App ++ : Submit login info
App -> db ++ : Validate login credentials
App <-- db -- : Login verified successfully
User <-- App --: Login successful

User -> App ++ : Inputs about their city data
App -> App: Verifies data
App -> db: Queries DB
App -> db: The data is stored in Database

db ->App:Returns Queried data
App -> User: User gets the value

@enduml
```
