```plantuml
@startuml
title Unsuccessful To share metrics

participant User
participant App
database UserStore as db

User -> App ++ : Submit login info
App -> db ++ : Validate login credentials
App <-- db -- : Login verified successfully
User <-- App --: Login successful

User -> App ++ : Inputs about their city data
App -> App: Verifies data

App -> db: The data is not stored in Database. Wrong datatype

@enduml
```
