```plantuml
@enduml
@startuml
title Successful Login

participant User
participant App
database UserStore as db

User -> App ++ : Submit login info
App -> db ++ : Validate login credentials
App <-- db -- : Login verified successfully
User <-- App --: Login successful
@enduml
```
