```plantuml
@startuml
title Unsuccessful Login — Bad credentials

participant User
participant App
database UserStore as db

User -> App ++ : Enter login credentials
App -> db ++ : Validate login credentials
App <-- db -- : Login verified unsuccessfully
User <-- App -- : Login unsuccessful. Try again.
@enduml
```
