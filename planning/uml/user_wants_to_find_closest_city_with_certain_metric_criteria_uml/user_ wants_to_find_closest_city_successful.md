```plantuml
@startuml
title Successful To find closest City

participant User
participant App
database UserStore as db

App -> App :Provides the longitude and longitude of the user
db -> App: Returns data of all the cities longitude and latitude
App -> User: Queries data and find the cities in radius

User -> App ++ : Selects one city from the list of cities

User -> App ++ : Searches a keyword for metrics using filters
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: Searching Metrics

User -> App: Submits Values
App -> App: Verifies data
App -> db: Queries DB
db ->App:Returns Queried data
App -> User: User gets the value of metrics with certain parameter as required


@enduml
```