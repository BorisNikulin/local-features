```plantuml
@startuml
title Unsuccessful To find closest City

participant User
participant App
database UserStore as db

User -> App ++ : Searches a keyword for city
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: No Cities searched found

User -> App ++ : Searches a keyword for metrics
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: No Metrics searched found

User -> App: Submits Values

App -> User: User gets error. No data found


@enduml
```
