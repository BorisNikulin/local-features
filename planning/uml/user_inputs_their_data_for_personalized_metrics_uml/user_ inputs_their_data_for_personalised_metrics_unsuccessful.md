```plantuml
@startuml
title Unsuccessful User inputs data

participant User
participant App
database UserStore as db


User -> App ++ : Submit login info
App -> db ++ : Validate login credentials
App <-- db -- : Login verified successfully
User <-- App --: Login successful

User -> App ++ : Inputs about their preferences
App -> db: Queries DB
App -> db: The data is not stored in Database due to wrong input

App -> User: User gets no preferences in the app


@enduml
```