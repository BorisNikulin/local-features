```plantuml
@startuml
title Successful Compare Metrics

participant User
participant App
database UserStore as db

User -> App ++ : Searches a keyword for city
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: Searching Cities

User -> App ++ : Searches a keyword for metrics
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: Searching Metrics

User -> App: Submits Values
App -> App: Verifies data 
App -> db: Queries DB
db ->App:Returns Queried data
App-> App: Compares data of the metrics
App -> User: User gets the value


@enduml
```
