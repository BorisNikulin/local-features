```plantuml
@startuml
title Unsuccessful To see metrics of particular city

participant User
participant App
database UserStore as db

User -> App ++ : Searches a keyword for one city
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: No city searched found

User -> App ++ : Searches a keyword for metrics
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: No Metrics searched found 

User -> App: Submits Values
App -> App: Verifies data
App -> User: User gets no value found 




@enduml
```