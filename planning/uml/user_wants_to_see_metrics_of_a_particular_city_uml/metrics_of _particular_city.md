```plantuml
@startuml
title Successful To see metrics of particular city

participant User
participant App
database UserStore as db

User -> App ++ : Searches a keyword for one city
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: Searching Cities

User -> App ++ : Searches a keyword for metrics
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: Searching Metrics

User -> App: Submits Values
App -> App: Verifies data
App -> db: Queries DB
db ->App:Returns Queried data
App -> User: User gets the metrics list 




@enduml
```
