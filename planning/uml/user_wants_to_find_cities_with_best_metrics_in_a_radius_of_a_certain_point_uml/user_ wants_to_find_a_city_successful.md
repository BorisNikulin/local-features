```plantuml
@startuml
title Successful User want to find a city

participant User
participant App
database UserStore as db


User -> App : User searhes on the map pointing circle
App -> User: Asks for one more circle on the map for radius
db -> App: Returns data of all the cities longitude and latitude
App -> User: Queries data and find the cities in radius

User -> App ++ : Selects one city from the list of cities

User -> App ++ : Searches a keyword for metrics
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: Searching Metrics

User -> App: Submits Values
App -> App: Verifies data
App -> db: Queries DB
db ->App:Returns Queried data
App -> User: User gets the value


@enduml
```