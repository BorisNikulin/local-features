```plantuml
@startuml
title Unsuccessful Login — Bad credentials

participant User
participant App
database UserStore as db

User -> App ++ : Enter sign up info
App -> db ++ : Validate sign up info requirements
App <-- db -- : Sign up verified unsuccessfully
User <-- App -- : Sign up unsuccessful. Try again.
@enduml
```
