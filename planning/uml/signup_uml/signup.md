```plantuml
@startuml
title Successful Login - Sign Up

participant User
participant App
database UserStore as db

User -> App ++ : Submit sign up info
App -> App: Validate sign up info
App -> db: Store new account
User <-- App -- : Redirect to account profile
@enduml
```
