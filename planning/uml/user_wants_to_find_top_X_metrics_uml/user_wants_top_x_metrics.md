```plantuml
@startuml
title Successful To find top x metrics

participant User
participant App
database UserStore as db



User -> App ++ : Searches a keyword for metrics
App -> db: SearchByKeyword(keyword)
db -> App: getPostList[]
App -> User: Searching Metrics

User -> App: Submits Values

App -> App: Verifies data
App -> db: Queries DB
db ->App:Returns Queried data
App -> User: User get 10 cities with requested metrics


@enduml
```
