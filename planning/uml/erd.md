```plantuml
@startuml

entity users {
    * userame
    ---
    * password
}

entity city {
    * city_id <<generated>>
    ---
    * name
}


entity metric {
    * metric_id <<generated>>
    ---
    * city_id <<FK>>
    * date_created
    * type
    * value
}

city ||--o{ metric
@enduml
```
