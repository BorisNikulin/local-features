#!/usr/bin/env python

import os
import json

# available only if job is for MRs only and job is for an MR
mr_iid = os.getenv('CI_MERGE_REQUEST_IID')
review_arg = f' --review.mr_iid={mr_iid}' if mr_iid is not None else ''

heroku_proc_types = {
    'web': f'./openjdk/bin/java -jar -Dspring.profiles.active=dev LocalFeatures*.jar --server.port=$PORT{review_arg}'
}

with open('process_types.json', 'w') as proc_types_file:
    json.dump(heroku_proc_types, proc_types_file)
