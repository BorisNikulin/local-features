#!/usr/bin/env python

import sys
import os
import http.client
import urllib.request
import json
import hashlib

heroku_api_name = sys.argv[1]

# requires these custom environmnet variables
heroku_api_key = os.environ['HEROKU_API_KEY']
heroku_url_base = os.environ['HEROKU_URL_BASE']

heroku_api_con = http.client.HTTPSConnection(heroku_url_base)
heroku_headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/vnd.heroku+json; version=3',
    'Authorization': f'Bearer {heroku_api_key}'
}

heroku_api_con.request(
    'DELETE',
    f'/apps/{heroku_api_name}',
    headers=heroku_headers
)

print(json.dumps(json.loads(heroku_api_con.getresponse().read()), indent=4))
heroku_api_con.close()
