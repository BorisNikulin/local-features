package com.localfeatures;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.localfeatures.database.City;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@DataJpaTest
@Import({CityServiceImpl.class, GeometryFactoryConfig.class})
public class CityServiceImplTest
{
    /**
     * Object under test.
     */
    @Autowired
    private CityService cityService;

    /**
     * Check finding by exact city.
     */
    @Test
    public void exactFindFound()
    {
        Optional<City> city = cityService.findCity("Apple");
        assertEquals(city.map(City::getName), Optional.of("Apple"));
        assertNotNull(city.map(City::getId).get());
    }

    /**
     * Check finding by partial city.
     */
    @Test
    public void fuzzyFindFound()
    {
        final Set<String> cities =
            cityService.findCities("ap")
                .stream()
                .map(City::getName)
                .collect(Collectors.toSet());

        final Set<String> foundCities =
            new HashSet<String>(
                    Arrays.asList(
                        "Apple",
                        "Pineapple",
                        "Grapefruit"));

        assertEquals(cities, foundCities);

    }

    /**
     * Check fuzzy finding by partial city.
     */
    @Test
    public void fuzzyPagedFindFound()
    {
        final var sort =
            Sort.sort(City.class)
                    .by(City::getName)
                    .ascending();
        final var pageRequest = PageRequest.of(0, 2, sort);

        final Page<City> cityPage1 =
            cityService.findCities("ap", pageRequest);
        final Page<City> cityPage2 =
            cityService.findCities("ap", cityPage1.nextPageable());

        final List<String> cities1 =
            cityPage1
                .stream()
                .map(City::getName)
                .collect(Collectors.toList());

        final List<String> cities2 =
            cityPage2
                .stream()
                .map(City::getName)
                .collect(Collectors.toList());

        final List<String> foundCities1 =
                    Arrays.asList(
                        "Apple",
                        "Grapefruit");

        final List<String> foundCities2 =
                    Arrays.asList(
                        "Pineapple");

        assertEquals(cities1, foundCities1);
        assertEquals(cities2, foundCities2);

        assertEquals(2, cityPage1.getNumberOfElements());
        assertEquals(1, cityPage2.getNumberOfElements());

        assertTrue(cityPage1.hasNext());
        assertFalse(cityPage2.hasNext());
    }
}
