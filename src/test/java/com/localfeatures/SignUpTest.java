package com.localfeatures;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.localfeatures.database.User;
import com.localfeatures.database.UserRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@AutoConfigureMockMvc
@SpringBootTest
public class SignUpTest
{
    /** Web application context. */
    @Autowired
    private WebApplicationContext context;

    /** Mock server. */
    private MockMvc mockMvc;

    /** Repository for User entities. */
    @Autowired
    private UserRepository userRepo;

    /** Password encoder used for auth and encoding. */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Sets up mock server to work with spring security.
     *
     * Due to using {@code @SpringBootTest}
     * spring security needs to be fully configured
     * or the mock server returns 403s.
     */
    @BeforeEach
    public void setup()
    {
        // I do not fully understand how this works
        // or why the myriad of other suggestions online do not.
        mockMvc =
            MockMvcBuilders.webAppContextSetup(context).build();
    }

    /**
     * Tests a successful sign up request.
     *
     * @throws Exception from mock server
     */
    @Transactional
    @Test
    public void signUpSuccess() throws Exception
    {
        final var signUpForm = new SignUpForm();
        signUpForm.setUsername("signUpSuccessUsername" + Math.random());
        signUpForm.setPassword("signUpSuccessPassword" + Math.random());
        signUpForm.setPasswordConfirmation(signUpForm.getPassword());

        System.out.println(signUpForm.toString());

        var result = mockMvc.perform(
            post("/sign-up")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", signUpForm.getUsername())
                .param("password", signUpForm.getPassword())
                .param("passwordConfirmation",
                    signUpForm.getPasswordConfirmation())
                )
            .andReturn();

        System.out.println(result.getResponse().toString());

        final User newUser = userRepo.findById(signUpForm.getUsername()).get();

        assertNotNull(newUser);
        assertTrue(passwordEncoder
            .matches(signUpForm.getPassword(), newUser.getPassword()));
    }


}
