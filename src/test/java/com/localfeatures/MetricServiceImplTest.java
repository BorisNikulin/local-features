package com.localfeatures;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.localfeatures.database.City;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

@DataJpaTest
@Import(MetricServiceImpl.class)
public class MetricServiceImplTest
{
    /**
     * Object under test.
     */
    @Autowired
    private MetricService metricService;

    /**
     * Check comparing two cities.
     */
    @Test
    public void compareCities()
    {
        final City apple = new City();
        final City orange = new City();

        final int metricLeftId = 27;
        final int metricRightId = 29;
        final Double deltaValue = 5.0 / 9.0;
        final double tolerance = 0.0000000001;

        apple.setId(1);
        orange.setId(2);

        CityDeltas cityDeltas = metricService.compareCities(apple, orange);

        assertTrue(cityDeltas.getDeltas().stream()
            .anyMatch(delta ->
                delta.getMetricLeft().getId() == metricLeftId
                && delta.getMetricRight().getId() == metricRightId
                && Math.abs(delta.getDelta() - deltaValue) < tolerance));
    }
}
