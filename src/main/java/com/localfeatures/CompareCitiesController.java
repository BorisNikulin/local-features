package com.localfeatures;

import java.util.Optional;

import com.localfeatures.database.City;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Controller
public class CompareCitiesController
{
    /**
     * Service for manipulating
     * {@link com.localfeatures.database.City}
     * objects.
     */
    private final CityService cityService;
    /**
     * Service for manipulating
     * {@link com.localfeatures.database.Metric}
     * objects.
     */
    private final MetricService metricService;

    /**
     * Compare two cities input form.
     *
     * @return form to enter cities to compare
     */
    @GetMapping("/compare-cities")
    public String compareCities()
    {
        return "compare_cities";
    }
    /**
     * Compare two cities by
     * showing the differences in their metrics.
     *
     * @param cityNameLeft the comparison subject
     * @param cityNameRight city we are comparing against
     * @param model model that the view uses
     *
     * @return the view displaying the comparison
     */
    @GetMapping(path = "/compare-cities", params = {"left_city", "right_city"})
    public String compareCities(
        @RequestParam(name = "left_city") final String cityNameLeft,
        @RequestParam(name = "right_city") final String cityNameRight,
        final Model model)
    {
        Optional<City> cityLeft = cityService.findCity(cityNameLeft);
        Optional<City> cityRight = cityService.findCity(cityNameRight);

        CityDeltas cityDeltas =
            metricService.compareCities(cityLeft.get(), cityRight.get());

        model.addAttribute("cityDeltas", cityDeltas);

        return "compare_cities_result";
    }
}
