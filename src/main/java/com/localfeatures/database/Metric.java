package com.localfeatures.database;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;
import lombok.ToString;

/**
 * Represents a metric for a city.
 *
 * This class is the superclass
 * of all metrics.
 *
 * The type of children clases
 * deternmines the type of metric represented.
 */
@Data
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type",
    discriminatorType = DiscriminatorType.STRING)
public abstract class Metric
{
    /**
     * Generated metric id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "metric_id")
    private int id;

    /**
     * City the metric is for.
     */
    @ToString.Exclude
    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "city_id")
    @NotNull
    private City city;

    /**
     * When the metric was sampled or obtained.
     */
    @NotNull
    private ZonedDateTime dateCreated;

    /**
     * The value of metric.
     *
     * Interpretation of the value depends
     * on the type of the child class.
     */
    @NotNull
    private Double value;
}
