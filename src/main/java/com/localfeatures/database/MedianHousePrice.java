package com.localfeatures.database;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Concrete metric for Median House prices.
 */
@Entity
@DiscriminatorValue("median_house_price")
public class MedianHousePrice extends Metric
{
}
