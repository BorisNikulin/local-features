package com.localfeatures.database;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MetricRepository
    extends CrudRepository<Metric, Integer>
{
    /**
     * Gets latest metrics per type for a city.
     *
     * @param city city to fetch latest metrics for
     *
     * @return list of latest metrics
     */
    @Query(
        "SELECT m1 " +
        "FROM Metric m1 " +
        "LEFT JOIN Metric m2 ON " +
            "m1.city = m2.city AND " +
            "m1.class = m2.class AND " +
            "m1.dateCreated < m2.dateCreated " +
        "WHERE " +
            "m1.city = :city AND " +
            "m2 IS NULL"
    )
    List<Metric> findLatestMetrics(@Param("city") City city);
}
