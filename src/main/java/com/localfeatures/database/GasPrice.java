package com.localfeatures.database;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Concrete metric for gas prices.
 */
@Entity
@DiscriminatorValue("gas_price")
public class GasPrice extends Metric
{
}
