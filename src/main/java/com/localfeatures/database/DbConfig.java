package com.localfeatures.database;

import java.net.URI;
import java.net.URISyntaxException;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariDataSource;

import lombok.extern.slf4j.Slf4j;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Bean;

@Slf4j
@Configuration
public class DbConfig
{
    /**
     * Defines pooled database connections to Heroku postgres.
     *
     * Taken directly from heroku on 2019-10-20.
     *
     * @throws NullPointerException If the
     *      {@code DATABASE_URL} environtment variable
     *      is not set
     * @throws URISyntaxException If provided DB url,
     *      via a system variable,
     *      is malformed
     *
     * @return Pooled source of database connections
     *
     * @see <a href="https://devcenter.heroku.com/articles/heroku-postgresql#connecting-in-java">
     *          Heroku Postgres Connection Guide
     *      </a>
     */
    @Profile("prod")
    @Bean
    public DataSource dataSource() throws URISyntaxException
    {
        log.info("Creating heroku postgres dataSource");

        var dbUri = new URI(System.getenv("DATABASE_URL"));

        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String jdbcUrl =
            "jdbc:postgresql://"
            + dbUri.getHost()
            + ':'
            + dbUri.getPort()
            + dbUri.getPath()
            + "?sslmode=require";

        var dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(jdbcUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }
}
