package com.localfeatures.database;

import java.util.List;
import java.util.Optional;

import org.locationtech.jts.geom.Geometry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository
    extends PagingAndSortingRepository<City, Integer>
{
    /**
     *  Maybe find a ciy by exact name.
     *
     *  @param name exact city name
     *
     *  @return maybe the city with given name
     */
    Optional<City> findByName(String name);

    /**
     * Find cities where the city contains the partial name ignoring case.
     *
     * @param partialName case insensitive partial city name
     *
     * @return list of cities having names containing the partial name
     *
     * @see #findByNameIgnoreCaseContaining(String, Pageable)
     */
    List<City> findByNameIgnoreCaseContaining(String partialName);

    /**
     * Find a page of cities
     * where the city contains the partial name ignoring case.
     *
     * Paged version of {@link #findByNameIgnoreCaseContaining(String)}.
     *
     * @param partialName case insensitive partial city name
     * @param pageable paging and or sorting parameters
     *
     * @return list of cities having names containing the partial name
     *
     * @see #findByNameIgnoreCaseContaining(String)
     */
    Page<City> findByNameIgnoreCaseContaining(
        String partialName,
        Pageable pageable);

    /**
     * Find cities with a given search space.
     *
     * @param searchSpace geomtry to search cities within
     *
     * @return list of cities in the search space
     */
    @Query(name = "City.findWithin")
    List<City> findWithin(
        @Param("searchSpace") Geometry searchSpace);

    /**
     * Paged version of
     * {@link #findWithin}.
     *
     * @param searchSpace geomtry to search cities within
     * @param pageable page settings
     *
     * @return page of cities in the search space
     */
    @Query(name = "City.findWithin")
    Page<City> findWithin(
        @Param("searchSpace") Geometry searchSpace,
        Pageable pageable);
}
