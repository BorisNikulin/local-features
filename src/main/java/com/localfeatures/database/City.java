package com.localfeatures.database;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import org.locationtech.jts.geom.Point;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents a city that may have associated metrics.
 */
@Data
@NoArgsConstructor
@Entity
@NamedQuery(name = "City.findWithin",
    query =
        "SELECT c " +
        "FROM City c " +
        "WHERE WITHIN(c.location, :searchSpace) = TRUE"
)
public class City
{
    /**
     * Generated city id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "city_id")
    private int id;

    /**
     * The city's name.
     */
    @NotNull
    private String name;

    /**
     * Location of the city.
     */
    @JsonIgnore
    @NotNull
    private Point location;

    /**
     * Zero to many associated metrics.
     */
    @JsonBackReference
    @OneToMany(mappedBy = "city")
    private List<Metric> metrics;

    /**
     * Construct city with given name an null id.
     *
     * @param cityName name of the city
     * @param cityLocation location of the city
     */
    public City(final String cityName, final Point cityLocation)
    {
        name = cityName;
        location = cityLocation;
    }
}
