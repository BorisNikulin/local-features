package com.localfeatures.database;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Concrete metric for Education prices.
 */
@Entity
@DiscriminatorValue("education_price")
public class EducationPrice extends Metric
{
}
