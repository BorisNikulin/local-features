package com.localfeatures.database;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Concrete metric for Hotel prices.
 */
@Entity
@DiscriminatorValue("hotel_price")
public class HotelPrice extends Metric
{
}
