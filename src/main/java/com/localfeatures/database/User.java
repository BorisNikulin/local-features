package com.localfeatures.database;

import java.util.Collection;
import java.util.Collections;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;

/**
 * Represents a registered user.
 */
@Data
@Entity
@Table(name = "users")
public class User implements UserDetails
{

    private static final long serialVersionUID = 1L;

    /**
     * Maximum characters for a username.
     */
    private static final int USERNAME_MAX_CHARS = 32;

    /**
     * Minimum characters for a username.
     */
    private static final int USERNAME_MIN_CHARS = 2;

    /**
     * Username and id of a user.
     */
    @Size(min = USERNAME_MIN_CHARS, max = USERNAME_MAX_CHARS)
    @NotNull
    @Id
    private String username;

    /**
     * Hashed password for user.
     *
     * May also encode hashing metadata and salt.
     * Depends on
     * {@link org.springframework.security.crypto.password.PasswordEncoder}
     * used.
     */
    @NotNull
    private String password;

    @Override
    public String getUsername()
    {
        return username;
    }

    @Override
    public String getPassword()
    {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return Collections.singleton(new SimpleGrantedAuthority("ROLE_USER"));
    }

    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    @Override
    public boolean isEnabled()
    {
        return true;
    }
}
