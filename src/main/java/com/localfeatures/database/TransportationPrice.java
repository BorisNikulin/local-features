package com.localfeatures.database;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Concrete metric for Transportation prices.
 */
@Entity
@DiscriminatorValue("transportation_price")
public class TransportationPrice extends Metric
{
}
