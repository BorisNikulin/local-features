package com.localfeatures.database;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Concrete metric for Grocery prices.
 */
@Entity
@DiscriminatorValue("grocery_price")
public class GroceryPrice extends Metric
{
}
