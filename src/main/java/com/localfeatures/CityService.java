package com.localfeatures;

import java.util.List;
import java.util.Optional;

import com.localfeatures.database.City;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lombok.NonNull;

/**
 * Service interface for handling and operating
 * {@link com.localfeatures.database.City} objects.
 */
public interface CityService
{
    /**
     * Try to find city by exact name.
     *
     * @param name non null exact city name
     *
     * @return may return a city
     */
    Optional<City> findCity(@NonNull String name);

    /**
     * Fuzzilly find cities by partial name.
     *
     * @param partialCityName non null partial city name
     *
     * @return all cities fuzzilly matching the partial name
     */
    List<City> findCities(@NonNull String partialCityName);

    /**
     * Paged version of {@link #findCities}.
     *
     * @param partialCityName non null partial city name
     * @param pagable non null paging settings
     *
     * @return all cities fuzzilly matching the partial name
     */
    Page<City> findCities(
        @NonNull String partialCityName,
        @NonNull Pageable pagable);

    /**
     * Find cities in a circle defined by center and radius of the circle.
     *
     * The coordinates should be consistent with the SRID used in
     * {@link GoemetryFactoryConfig#geometryFacotry()}.
     * Note that due to the circle being represented via raster,
     * precision near the edges may be sup par compared to a true circle.
     *
     * @param x x coordinate of the circle's center
     * @param y y coordinate of the circle's center
     * @param radius radius of the cricle
     *
     * @return cities inside the circle
     */
    List<City> findCitiesInCircle(
       @NonNull Double x,
       @NonNull Double y,
       @NonNull Double radius);

    /**
     * Paged version of {@link #findCitiesInCircle}.
     *
     * @param x x coordinate of the circle's center
     * @param y y coordinate of the circle's center
     * @param radius radius of the cricle
     * @param pageable paging settings
     *
     * @return cities inside the circle
     */
    Page<City> findCitiesInCircle(
       @NonNull Double x,
       @NonNull Double y,
       @NonNull Double radius,
       @NonNull Pageable pageable);
}
