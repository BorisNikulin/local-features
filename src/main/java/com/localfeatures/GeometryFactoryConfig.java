package com.localfeatures;

import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class GeometryFactoryConfig
{
    /** SRID for WGS84 CRS. */
    private static final int WGS84 = 4326;

    /**
     * GeometryFactory with WGS84 CRS.
     *
     * @return the factory
     */
    @Bean
    @Scope("prototype")
    public GeometryFactory geometryFactory()
    {
        return new GeometryFactory(new PrecisionModel(), WGS84);
    }
}
