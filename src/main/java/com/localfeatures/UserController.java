package com.localfeatures;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.RequiredArgsConstructor;

/**
 * Controller for user related tasks.
 */
@RequiredArgsConstructor
@Controller
public class UserController
{
    /**
     * Service for manipulating
     * {@link com.localfeatures.database.User}
     * objects.
     */
    private final UserService userService;
    /**
     * Validator for
     * {@link SignUpForm}
     * objects.
     */
    private final SignUpFormValidator signUpFormValidator;

    /**
     * Custom data binder augmented with sign up form validation.
     *
     * @param binder data binder to augment
     */
    @InitBinder
    private void signUpInitBinder(final WebDataBinder binder)
    {
        binder.addValidators(signUpFormValidator);
    }

    /**
     * Sign up form.
     *
     * @param model model that the view uses
     *
     * @return the sign up form
     */
    @GetMapping("/sign-up")
    public String signUp(final Model model)
    {
        model.addAttribute("signUp", new SignUpForm());
        return "sign_up_form";
    }

    /**
     * Sign up form submission endpoint.
     *
     * Validates sign up form
     * and either presents an error
     * or registers the new user.
     *
     * @param signUpForm the sign up form
     * @param result validation results
     *
     * @return either a success view
     * or the form again with errors
     */
    @PostMapping("/sign-up")
    public String signUp(
        @Validated @ModelAttribute(name = "signUp") final SignUpForm signUpForm,
        final BindingResult result)
    {
        if (result.hasErrors())
        {
            return "sign_up_form";
        }

        userService.registerUser(signUpForm.toUser());

        return "sign_up_success";
    }
}
