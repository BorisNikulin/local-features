package com.localfeatures;

import com.localfeatures.database.User;
import com.localfeatures.database.UserRepository;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Service interface for handling and operating on
 * {@link com.localfeatures.database.User}
 * objects.
 */
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService
{
    /**
     * Repository for
     * {@link com.localfeatures.database.User}
     * entities.
     */
    private final UserRepository userRepo;
    /** Password encoder used for auth and encoding. */
    private final PasswordEncoder passwordEncoder;

    @Override
    public void registerUser(@NonNull final User newUser)
    {
        final String encodedPassword =
            passwordEncoder.encode(newUser.getPassword());

        newUser.setPassword(encodedPassword);

        userRepo.save(newUser);
   }
}
