package com.localfeatures;

import com.localfeatures.database.City;

import lombok.NonNull;

/**
 * Service interface for handling and operating
 * {@link com.localfeatures.database.Metric}
 * objects.
 */
public interface MetricService
{
    /**
     * Compare the metrics of two cities
     * and create an object detailing
     * such a comparison.
     *
     * @param cityLeft the subject of comparison
     * @param cityRight the city being compared against
     *
     * @return result of comparing each metric
     * of the first city agaisnt the second
     */
    CityDeltas compareCities(
        @NonNull City cityLeft,
        @NonNull City cityRight);
}
