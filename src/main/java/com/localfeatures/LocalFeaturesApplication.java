package com.localfeatures;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocalFeaturesApplication
{

    /**
     * Main function.
     * Uses spring to run the app.
     *
     * @param args command line arguments
     */
    public static void main(final String[] args)
    {
        SpringApplication.run(LocalFeaturesApplication.class, args);
    }
}
