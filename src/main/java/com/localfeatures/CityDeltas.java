package com.localfeatures;

import java.util.Collection;

import com.localfeatures.database.City;
import com.localfeatures.database.Metric;

import lombok.Data;

/**
 * Represeents a comparison of metrics
 * between two cities.
 */
@Data
public class CityDeltas
{
    /** City that is the subject of comparison.*/
    private final City cityLeft;
    /** City that is being compared against.*/
    private final City cityRight;

    /** Comparison of metrics that both cities have. */
    private final Collection<Delta> deltas;

    /** Metrics the left city has that the right does not. */
    private final Collection<Metric> cityLeftUnmatched;
    /** Metrics the right city has that the left does not. */
    private final Collection<Metric> cityRightUnmatched;
}
