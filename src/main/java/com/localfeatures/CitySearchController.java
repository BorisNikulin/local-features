package com.localfeatures;

import java.util.List;

import com.localfeatures.database.City;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Controller
public class CitySearchController
{
    /**
     * Service for manipulating
     * {@link com.localfeatures.database.City}
     * objects.
     */
    private final CityService cityService;

    /**
     * Form for searching for cities.
     *
     * @return form for searching for cities
     */
    @GetMapping("/city/search")
    public String searchCities()
    {
        return "city_search";
    }

    /**
     * Find cities within a given circle.
     *
     * @param x x coordinate of the circle's center
     * @param y y coordinate of the circle's center
     * @param radius radius of the cricle
     * @param model model that the view uses
     *
     * @return view of found cities inside the circle
     */
    @GetMapping(path = "/city/search",
        params = {"x", "y", "radius"})
    public String searchByCircle(
        @RequestParam final double x,
        @RequestParam final double y,
        @RequestParam final double radius,
        final Model model)
    {
        List<City> cities = cityService.findCitiesInCircle(x, y, radius);
        model.addAttribute("cities", cities);

        return "cities";
    }
    /**
     * Find cities with a given name.
     *
     * @param name name of the city to search for
     * @param model model that the view uses
     *
     * @return view of found cities inside the circle
     */
    @GetMapping(path = "/city/searchname", params = {"name"})
    public String searchByName(
        @RequestParam final String name, final Model model)
    {
        List<City> cities = cityService.findCities(name);
        model.addAttribute("cities", cities);
        return "cities";
    }
}
