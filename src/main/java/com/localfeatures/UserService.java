package com.localfeatures;

import com.localfeatures.database.User;

import lombok.NonNull;

/**
 * Service interface for handling and operating on
 * {@link com.localfeatures.database.User}
 * objects.
 */
public interface UserService
{
    /**
     * Register a new user uncoditionally.
     *
     * One should use the
     * {@link SignUpFormValidator}
     * in a transaction
     * to ensure sucess of registration.
     *
     * @param newUser user to register
     */
    void registerUser(@NonNull User newUser);
}
