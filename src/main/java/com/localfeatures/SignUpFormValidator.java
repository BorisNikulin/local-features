package com.localfeatures;

import com.localfeatures.database.UserRepository;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.ValidationUtils;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class SignUpFormValidator implements Validator
{
    /**
     * Repository for
     * {@link com.localfeatures.database.User}
     * entities.
     */
    private final UserRepository userRepo;

    @Override
    public boolean supports(final Class<?> clazz)
    {
        return SignUpForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors)
    {
        final var signUpForm = (SignUpForm) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,
            "username", "sign_up.error.username_required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,
            "password", "sign_up.error.password_required");

        if (signUpForm.getUsername().strip().length() < 2)
        {
            errors.rejectValue("username",
                "sign_up.error.short_username");
        }

        if (!signUpForm
                .getPassword()
                .equals(signUpForm.getPasswordConfirmation()))
        {
            errors.rejectValue("passwordConfirmation",
                "sign_up.error.password_mismatch");
        }

        if (userRepo.existsById(signUpForm.getUsername()))
        {
            errors.rejectValue("username",
                "sign_up.error.username_taken");
        }
    }
}
