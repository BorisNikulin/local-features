package com.localfeatures;

import lombok.Data;

@Data
class CitiesForm
{
  /** First city user has selected. */
  private String city1;

  /** Second city user has selected. */
  private String city2;
}
