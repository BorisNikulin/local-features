package com.localfeatures;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import com.localfeatures.database.City;
import com.localfeatures.database.Metric;
import com.localfeatures.database.MetricRepository;

import org.springframework.stereotype.Service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Service interface for handling and operating
 * {@link com.localfeatures.database.Metric}
 * objects.
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class MetricServiceImpl implements MetricService
{
    /**
     * Repository for
     * {@link com.localfeatures.database.Metric}
     * entities.
     */
    private final MetricRepository metricRepo;

    @Override
    public CityDeltas compareCities(
        @NonNull final City cityLeft,
        @NonNull final City cityRight)
    {
        log.info("Comparing {} to {}", cityLeft.getName(), cityRight.getName());

        List<Metric> metricsLeft = metricRepo.findLatestMetrics(cityLeft);
        List<Metric> metricsRight = metricRepo.findLatestMetrics(cityRight);

        LinkedHashMap<Class<?>, Metric> metricsLeftIndexed =
            metricsLeft.stream()
            .collect(
                LinkedHashMap::new,
                (map, metric) -> map.put(metric.getClass(), metric),
                LinkedHashMap::putAll);

        var deltas = new LinkedList<Delta>();
        var metricsRightUnmatched = new LinkedList<Metric>();

        for (var metricRight : metricsRight)
        {
            Metric metricLeft =
                metricsLeftIndexed.get(metricRight.getClass());
            if (metricLeft != null)
            {
                Double delta =
                    metricRight.getValue() == 0 ?
                    null :
                    metricLeft.getValue() / metricRight.getValue();

                deltas.add(new Delta(metricLeft, metricRight, delta));
                metricsLeftIndexed.remove(metricLeft.getClass());
            }
            else
            {
                metricsRightUnmatched.add(metricRight);
            }
        }

        return new CityDeltas(
                cityLeft,
                cityRight,
                deltas,
                metricsLeftIndexed.values(),
                metricsRightUnmatched);
    }
}
