package com.localfeatures.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Profile("dev")
@Order(DevWebSecurityConfig.LOAD_PRECEDENCE)
@Configuration
@EnableWebSecurity
public class DevWebSecurityConfig extends WebSecurityConfigurerAdapter
{

    /** Prescedence with which to determine when to load this config. */
    public static final int LOAD_PRECEDENCE = 5;

    @Override
    protected void configure(final HttpSecurity http)
        throws Exception
    {
        http
            .antMatcher("/h2-console/**")
                .csrf().disable()
                .headers().frameOptions().disable()
                .and()
            .authorizeRequests()
            .antMatchers("/h2-console/**")
                .permitAll();
    }
}
