package com.localfeatures.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Order(Ordered.LOWEST_PRECEDENCE)
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter
{
    @Override
    protected void configure(final HttpSecurity http)
        throws Exception
    {
        http
            .authorizeRequests()
                .anyRequest().permitAll()
                .and()
            .formLogin();
    }

    /**
     * Create DelegatingPasswordEncoder.
     *
     * Supports "noop" and "argon2" encoders
     * using their namesake algorithms.
     *
     * @return a DelegatingPasswordEncoder
     */
    @Bean
    protected PasswordEncoder passwordEncoder()
    {
        var defaultEncoder = "argon2";
        Map<String, PasswordEncoder> encoders = new HashMap<>();

        encoders.put(defaultEncoder, new Argon2PasswordEncoder());
        encoders.put("noop", getNoopPasswordEncoderInstance());

        return new DelegatingPasswordEncoder(defaultEncoder, encoders);
    }

    /**
     * Use NoopPasswordEncoder while suppressing warnings.
     *
     * @return singleton instance of
     * {@link org.springframework.security.crypto.password.NoOpPasswordEncoder}
     */
    @SuppressWarnings("deprecation")
    private PasswordEncoder getNoopPasswordEncoderInstance()
    {
        return org.springframework.security.crypto.password
            .NoOpPasswordEncoder.getInstance();
    }
}
