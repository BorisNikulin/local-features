package com.localfeatures.security;

import com.localfeatures.database.UserRepository;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
    /**
     * Repository for
     * {@link com.localfeatures.database.User}
     * entities.
     */
    private final UserRepository userRepo;

    @Override
    public UserDetails loadUserByUsername(final String username)
        throws UsernameNotFoundException
    {
        return userRepo
            .findById(username)
            .orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
