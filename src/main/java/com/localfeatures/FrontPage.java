package com.localfeatures;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class FrontPage
{
    @GetMapping("/")
    public String index()
    {
        return "front_page";
    }
}
