package com.localfeatures;

import com.localfeatures.database.Metric;

import lombok.Data;

/**
 * Represents a specific comparison of metrics.
 */
@Data
public class Delta
{
    /** Subject metric of comparison. */
    private final Metric metricLeft;
    /**  Metric being compared against. */
    private final Metric metricRight;

    /** Multiplicative difference of the left compared to the right. */
    private final Double delta;
}
