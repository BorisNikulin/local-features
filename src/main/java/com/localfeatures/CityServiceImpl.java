package com.localfeatures;

import java.util.List;
import java.util.Optional;

import com.localfeatures.database.City;
import com.localfeatures.database.CityRepository;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.util.GeometricShapeFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Concrete implementaton backed by a repository.
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class CityServiceImpl implements CityService
{

    /**
     * Repository for City entities.
     */
    private final CityRepository cityRepo;

    /** Geometry factory with set SRID. */
    private final GeometryFactory geoFactory;

    @Override
    public Optional<City> findCity(@NonNull final String name)
    {
        log.info("Finding city '{}'", name);
        Optional<City> city = cityRepo.findByName(name);
        log.info(city.map(ignored -> "City found").orElse("City not found"));
        return city;
    }

    @Override
    public List<City> findCities(@NonNull final String partialCityName)
    {
        log.info("Finding partial city '{}'", partialCityName);
        List<City> cities =
            cityRepo.findByNameIgnoreCaseContaining(partialCityName);
        log.info("Found {} cities", cities.size());
        return cities;
    }

    @Override
    public Page<City> findCities(
            @NonNull final String partialCityName,
            @NonNull final Pageable pagable)
    {
        log.info("Finding partial city '{}'", partialCityName);
        Page<City> cities =
            cityRepo.findByNameIgnoreCaseContaining(partialCityName, pagable);
        log.info(
                "Found {} cities of {} total cities",
                cities.getNumberOfElements(),
                cities.getTotalElements()
        );
        return cities;
    }

    /**
     * Make a circle with given paramters.
     *
     * @param x x coordinate of the circle's center
     * @param y y coordinate of the circle's center
     * @param radius radius of the cricle
     *
     * @return polygon representing the circle
     */
    private Polygon makeCircle(
        @NonNull final Double x,
        @NonNull final Double y,
        @NonNull final Double radius)
    {
        final var shapeFactory = new GeometricShapeFactory(geoFactory);
        final var diameter = 2 * radius;

        shapeFactory.setCentre(new Coordinate(x, y));
        shapeFactory.setSize(diameter);
        return shapeFactory.createCircle();
    }

    @Override
    public List<City> findCitiesInCircle(
        @NonNull final Double x,
        @NonNull final Double y,
        @NonNull final Double radius)
    {
        return cityRepo.findWithin(makeCircle(x, y, radius));
    }

    @Override
    public Page<City> findCitiesInCircle(
        @NonNull final Double x,
        @NonNull final Double y,
        @NonNull final Double radius,
        @NonNull final Pageable pageable)
    {
        return cityRepo.findWithin(makeCircle(x, y, radius), pageable);
    }
}
