package com.localfeatures;

import com.localfeatures.database.User;

import lombok.Data;

@Data
public class SignUpForm
{
    /** Desired username. */
    private String username;
    /** Desired password. */
    private String password;
    /** Password confirmation. */
    private String passwordConfirmation;

    /**
     * Convert form to a user object.
     *
     * Form should be validated with
     * {@link SignUpFormValidator}
     * before conversion.
     *
     * @return user object created from sign up form
     */
    public User toUser()
    {
        final var user = new User();
        user.setUsername(username);
        user.setPassword(password);

        return user;
    }
}
