stages:
    - build_test
    - deploy

variables:
    GRADLE_USER_HOME: "${CI_PROJECT_DIR}/.gradle"
    GRADLE_OPTS: "-Dorg.gradle.daemon=false"

build-localfeatures:
    image: gradle:5.6.2-jdk11
    stage: build_test
    interruptible: true
    script:
        - gradle --build-cache bootjar
        - mkdir app
        - mv ./build/libs/LocalFeatures*.jar ./app
        - mv scripts/deploy.py app/deploy.py
        - mv scripts/process_types.json app/process_types.json
        - cp -r /opt/java/openjdk ./app
    dependencies: []
    cache:
        paths:
            - .gradle/cache
            - .gradle/wrapper
    artifacts:
        expire_in: 1 week
        paths:
            - app/
    only:
        - merge_requests
        - master

test-localfeatures:
    image: gradle:5.6.2-jdk11
    stage: build_test
    interruptible: true
    variables:
        COVERAGE_REGEX: "Total.*?([0-9]{1,3})%"
    script:
        - mkdir public
        - gradle test
        - mv ./build/reports/tests/test ./public/tests
        - gradle jacocoTestReport
        # output code coverage to standard out
        - cat build/reports/jacoco/test/html/index.html | grep -E ${COVERAGE_REGEX} -o
        - mv ./build/reports/jacoco/test/html ./public/code-coverage
        # run rules against code coverage to pass/fail job
        - gradle jacocoTestCoverageVerification
    coverage: "/Total.*?([0-9]{1,3})%/"
    dependencies: []
    cache:
        paths:
            - .gradle/cache
            - .gradle/wrapper
    artifacts:
        expose_as: 'test reports'
        paths:
            - public/
        reports:
            junit:
                - build/test-results/test/TEST-*.xml
                - build/test-results/test/**/TEST-*.xml
    only:
        - merge_requests

check-style-localfeatures:
    image: gradle:5.6.2-jdk11
    stage: build_test
    script:
        - gradle checkStyleMain
        - gradle checkStyleTest
        - mkdir public
        - mv ./build/reports/checkstyle ./public/checkstyle
    dependencies: []
    cache:
        paths:
            - .gradle/cache
            - .gradle/wrapper
    artifacts:
        paths:
            - public/
    only:
        - merge_requests

deploy-review:
    image: python:3.7.3-alpine
    stage: deploy
    interruptible: false
    environment:
        name: review/${CI_COMMIT_REF_NAME}
        url: https://${HEROKU_APP_NAME_SHORT}-${CI_COMMIT_REF_SLUG}.herokuapp.com
        on_stop: stop-review
    script:
        - export HEROKU_APP_NAME="${HEROKU_APP_NAME_SHORT}-${CI_COMMIT_REF_SLUG}"
        - ./scripts/heroku_new_app.py ${HEROKU_APP_NAME}
        - ./scripts/gen_review_process_types_json.py
        - mv -f ./process_types.json ./app
        - cd app
        - mkdir app
        - mv *.jar ./app
        - mv ./openjdk ./app
        - tar czfv slug.tgz ./app
        - ./deploy.py slug.tgz process_types.json
    dependencies:
        - build-localfeatures
    only:
        # required for CI_MERGE_REQUEST_IID to be defined
        - merge_requests

stop-review:
    stage: deploy
    interruptible: false
    environment:
        name: review/${CI_COMMIT_REF_NAME}
        action: stop
    variables:
        GIT_STRATEGY: none
    script:
        - >-
            curl
            -sS
            -X DELETE
            https://${HEROKU_URL_BASE}/apps/${HEROKU_APP_NAME_SHORT}-${CI_COMMIT_REF_SLUG}
            -H "Content-Type: application/json"
            -H "Accept: application/vnd.heroku+json; version=3"
            -H "Authorization: Bearer ${HEROKU_API_KEY}"
    dependencies: []
    when: manual
    only:
        - merge_requests


deploy-localfeatures:
    image: python:3.7.3-alpine
    stage: deploy
    interruptible: false
    environment:
        name: production
        url: https://local-features.herokuapp.com
    variables:
        GIT_STRATEGY: none
    script:
        - cd app
        - mkdir app
        - mv *.jar ./app
        - mv ./openjdk ./app
        - ls -lh --color=auto
        - tar czf slug.tgz ./app
        - ./deploy.py slug.tgz process_types.json
    dependencies:
        - build-localfeatures
    only:
        refs:
            - master

build-srs:
    image: aergus/latex
    stage: build_test
    interruptible: true
    script:
        - mkdir public
        - cd planning
        - latexmk -lualatex -interaction=nonstopmode srs.tex
        - cd ..
        - mv ./planning/srs.pdf ./public
    dependencies: []
    artifacts:
        expire_in: 1 week
        paths:
            - public/
pages:
    stage: deploy
    interruptible: false
    script:
        - echo "Deploying"
    dependencies:
        - check-style-localfeatures
        - test-localfeatures
        - build-srs
    artifacts:
        expire_in: 1 day
        paths:
            - public/
    only:
        refs:
            - master
