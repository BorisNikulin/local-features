# Living Features

A software engineering project
for creating an app to help compare places using various metrics and features.

# Website

The website can be found
[here](https://local-features.herokuapp.com).

# Software Services Requirements

The software services requirements can be found
[here](https://borisnikulin.gitlab.io/local-features/srs.pdf).

# Testing the Use Cases

The test for the use cases can be found
[here](https://docs.google.com/spreadsheets/d/1QiDse43t2wI8YvBi20741Q_-CJnjNzB0kGAC7LswFKI/edit?usp=sharing).

# Final Presentation

The final presentation slides can be found
[here](./report/Group_presentation.pdf).
